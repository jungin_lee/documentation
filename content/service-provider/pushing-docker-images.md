---
title: "Pushing Docker Images"
date: 2019-11-07T16:32:18+01:00
draft: false

weight: 20

tags:
    - docker
---

## Key facts

**Registry URL:** `eu.gcr.io/arbalo-service-providers/`  
**Location:** Europe  
**Authentication:** Service Accounts with JSON Key files obtained directly from Arbalo  

## Log in to registry

After being provided a JSON key file by Arbalo 

**Note:** there is a known conflict between Docker authentication and `gcloud` authentication. If the suggested Docker login below succeeds but pushing and pulling fails, please see the [Troubleshooting](#troubleshooting) section below.

Using modern Docker clients that support the `--password-stdin` option:

```bash
cat keyfile.json | docker login -u _json_key --password-stdin https://eu.gcr.io
```

If the Docker client complains that `--password-stdin` is not a valid option please try the following command:

```bash
docker login -u _json_key -p "$(cat keyfile.json)" https://eu.gcr.io
```

## Tagging images

When tagging images please use the service slug and your version of the service.

```bash
docker tag your-image:version eu.gcr.io/arbalo-service-providers/image-name:version
```

## Pushing images

After authenticating and tagging the image, push to the repository.

```bash
docker push eu.gcr.io/arbalo-service-providers/image-name:version
```

## Complete Example

```bash
cat keyfile.json | docker login -u _json_key --password-stdin https://eu.gcr.io

docker tag arbalo-api:v0.1.1 eu.gcr.io/arbalo-service-providers/arbalo-api:v0.1.1

docker push eu.gcr.io/arbalo-service-providers/arbalo-api:v0.1.1
```

## Troubleshooting

### `access denied for eu.gcr.io/arbalo-service-providers/...`

#### With `gcloud` installed

It is possible that the `gcloud` CLI tool and Docker helper is attempting to authenticate with the non-Arbalo credentials.
To try logging into `gcloud` with the same JSON file provided by Arbalo:

```bash
gcloud auth activate-service-account --key-file keyfile.json
```
