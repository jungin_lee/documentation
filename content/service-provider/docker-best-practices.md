---
title: "Docker Best Practices"
date: 2019-11-12T07:26:41+01:00
draft: false

weight: 10

tags:
    - docker
    - best practices
---

## Multi-stage builds for smaller images

Starting in Docker 17.05, it is possible to to use ["multi-stage" builds](https://docs.docker.com/develop/develop-images/multistage-build/).  Multi-stage allow us to use one Docker image to compile and package the source code and a second Docker image to run the prepared binary or code.

This provides a few key benefits:

1. **Small image sizes** - By only including the compiled binary and libraries - without any build tools, the compiler or development dependencies - it is possible to drastically reduce the size of the final Docker image.  Not only does this help save on storage and bandwidth costs but a smaller image size means it's faster to pull and deploy the software resulting in faster auto-scaling times.
2. **Reduced attack vector** - More libraries and dependencies lead to the higher probability of security holes and vulnerabilities. By packaging only what is needed at _runtime_ we limit the chances of exposing a security hole.

Getting started is very simple, as shown in the example below for a Go application.  By using the `golang:1.7.3` images (aliased to `builder`) to compile the source code and a lightweight `alpine:latest` image to copy and execute the compiled code.

```dockerfile
FROM golang:1.7.3 AS builder
WORKDIR /go/src/github.com/alexellis/href-counter/
RUN go get -d -v golang.org/x/net/html  
COPY app.go    .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

FROM alpine:latest  
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=builder /go/src/github.com/alexellis/href-counter/app .
CMD ["./app"]
```

## Using a non-`root` user

Docker by default runs as user `root` within the container however - to restrict access rights of a compromised container - it is good practice to create a non `root` user that runs within the container.

This can be done a couple of ways depending on the platform and combined to give best compatibility. In the simplest version only the executable needs to have permissions added when building the image however if some form of state is required it is important to set ownership and permissions on all directory and file dependencies.

### `Dockerfile`

When building the `Dockerfile`, Docker provides the useful [`USER`](https://docs.docker.com/engine/reference/builder/#user) instruction.  This will set the user name (or UID) and an optional group name (or GID) to use when running subsequent commands both within the `Dockerfile` and consequently the `CMD` and `ENTRYPOINT`.

#### Example using `USER`

```dockerfile
FROM alpine:latest

ENV UID=100001 \
    USER=docker

# Create a user
RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "$(pwd)" \
    --no-create-home \
    --uid "$UID" \
    $USER

RUN chown -R $USER /path/to/files/requiring/permissions

USER docker
```

### Openshift

RedHats [Openshift](https://www.openshift.com/) enforces this practice by default. All containers are run with a unpredictable user ID (e.g. `--user 1000001`) and group `root`.

To make sure a container has the required permissions, it is important to set relevant ownership and permissions for group `root` when building the Dockerfile.

```dockerfile
# Make sure required files are owned by group `root` (user less important)
RUN chown -R 1001:root /path/to/files/requiring/permissions

# Give group permissions to files
RUN chmod -R 775 /path/to/files/requiring/permissions
```
