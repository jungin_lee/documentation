---
title: "Service Catalog"
date: 2019-11-124T21:45:52+01:00
draft: false

weight: 5
---

As a marketplace for cloud APIs and services the main place to browse and find services to use is the [Service Catalog](https://app.test.arbalo.dev/service/catalog).

![Service Catalog](service-catalog.png)

## Service Details

After selecting a service to use, click to see the Service Detail page.  From the Service Detail it is possible to select a price plan and provision an instance of the service for immediate use.

### Overview

Every service has a details page with some information about core functionality, release history, integration instructions and pricing.

As the usage of the platform increases each service will be augmented with additional metadata such as:

* Average latency - the time taken for request processing across all successful requests
* Failure rates - percentage of requests that produce a `5xx` HTTP error code to the caller (failure requests do not count as billable)

### Release Notes

> Currently used only as a placeholder

Contains a timeline of release notes as newer versions of software are released.  If you have a provisioned instance of this service you will automatically receive email notifications when a new version of the service becomes available on the Arbalo platform.

### Reviews

> Currently used only as a placeholder

Used a service and loved it? Found a service that has a high latency? 

Let users know in our reviews section and see reviews from other Arbalo users. Reviews can have a positive/neutral/negative rating and will help keep other users and the staff at Arbalo up to date with opinions of each service.

### Pricing

> During Beta phase all pricing is $0/transaction for our beta testers. This will change when the productive platform is available.

Each service may have multiple pricing options however at Arbalo we aim to make this as simple as possible. By aiming to assign a single transaction value to each service, bills become simple and predictable based on usage.

If there are multiple price plans available, each plan will display any unique features on the service detail page.

### Instances

Any currently provisioned instances of this service including their usage will appear here.

### API Documentation viewer

> **Coming soon**

Services with OpenAPI/Swagger documentation will have a built in documentation viewer to easily see the functionality and options available to the user.
