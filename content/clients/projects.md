---
title: "Projects"
date: 2019-11-12T07:45:31+01:00
draft: false

weight: 20
---

Projects are logical groupings of users and service instances. Each project has it's own single bill for all service instances and a given billing contact will receive a breakdown of all usage each month.

## Default Project

After registering, every user will start with a "Default" project to create instances in during their free trial phase.  Once the trial has finished, users will need to have valid billing information in their project to continue using the services they have provisioned otherwise access will be blocked.

## Adding a Project

From the "Default" project page it is possible to use the tab bar to "Add a Project".

By providing a name and valid billing details a new, empty project will be created ready to add other users, service instances and client credentials to.

> Only a name is needed during the beta phase to create a project with no service instances and just a single user as a collaborator.

## Billing Information

> Note: During the beta phase billing is disabled as pricing for services is currently free.  Prices will be updated and billing will be enabled after the general release

Add and update billing information under the project settings. Payment info and billing contacts are set _per project_ to give greater flexibility when organising groups of related services and users.

## Collaborators

> During the Beta phase projects are limited to a single user/collaborator

Other users (or collaborators) can be added to projects with ability to add service instances, client keys and other collaborators.  Press the "Add User" button in the "Collaborators" section and enter the email address of the user you wish to add to the project. If the user already has an Arbalo account they will automatically join the project. If they do not have an account the user will be invited to sign up first before joining the project.

## Removing a Project

To remove a project and disable all service instances and client credentials please contact Arbalo directly and we will de-provision all services. It is possible to request a future data to remove access.

Please note, by removing a project the following will also be removed:

* Service instances
* Client Credentials
* Collaborators
  * Collaborators will keep their accounts but will be unable to access the project

A final bill will be calculated at the end of the month for any usage before removal and sent to the billing contact for the project.
