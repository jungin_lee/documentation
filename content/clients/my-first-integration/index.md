---
title: "My First Integration"
date: 2019-11-12T07:45:46+01:00
draft: false

weight: 40
---

As a complete example we will use basic command line tools (`curl`) to connect to the Simple Time Service and retrieve the current time however the instructions here can be adapted to your favourite programming language.

## Prerequisites

* [Register](/clients/registration) as an Arbalo user
* Create a [Client ID & Client Secret](/clients/client-keys)

## Tutorial

In a only a few steps we are going to provision an instance of and connect to a service.  This can be done in any project but will require an active Client ID & Client Secret.

### 1. Provision a service instance

From the [service catalog](https://app.test.arbalo.dev/) select the "Simple Time Service" and provision a free instance which will appear in the Dashboard of your project.

**Save:** the URL for the newly created service instance

### 2. Get an Access Token

Using the client ID and client secret, make a request to our Identity Provider (IDP) to receive a short lived access token that will be used to access the service instance.

```shell
curl -X POST --silent --data "grant_type=client_credential" --user "{{ client ID }}:{{ client secret }}" "https://idp.arbalo.dev/auth/realms/arbalo.test/protocol/openid-connect/token"
```

All being well this should output a JSON object with an `"access_token"` key and value a long, Base 64 encoded JWT valid for 5 minutes.  Save this token for later.

> Bonus tip: Use [`jq`](https://stedolan.github.io/jq/) to parse the JSON in the command line and output only the `access_token`: Pipe to `| jq -r ".access_token | tostring"`

**Save:** the access token

### 3. Connect to our service instance

With the `access_token` we are ready to make a secure request to our service instance (and any other instance provisioned in the project). Add the access token to the `Authorization` HTTP header prefixed with the type `Bearer `.

```shell
curl -X GET --silent --header "Authorization: Bearer {{ access_token }}" https://{{ service instance url }}
```

Our expectation is the current time returned from our service instance in JSON format.

> Note: The Simple Time Service is the "Hello World" of integrations. For more advanced APIs the paths, data etc. all stay the same and can be used with no changes between instances. Only the instance ID and access tokens need to be modified.

#### Troubleshooting: `401` received

It is possible that the access token has expired. Access tokens have a short lifetime of 5 minutes and if a `HTTP 401` is returned it is possible that the token has expired. In this scenario there are two options:

1. Fetch a new access token [recommended in testing and demo situations like this one]
    Using the same client id and secret as before, simply make a request to be re-issued a new, valid access token.
2. Check token expiry time before making request [recommended in production systems]
    The access token is a [JWT](https://jwt.io/) that can be decoded and inspected using libraries in most popular programming languages.  Part of the token is an expiry time that can be checked either routinely or before each request. If the expiry time is in the past, the token is no longer valid and a new access token should be requested.  Using this method will ensure issuing requests are only made when necessary.

### Monitoring usage

Back on the Dashboard for the project the usage graph will now have some extra requests in it for today.
