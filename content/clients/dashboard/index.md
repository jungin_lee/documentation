---
title: "Dashboard"
date: 2019-11-14T21:05:43+01:00
draft: false

weight: 15
---

## Overview

The Dashboard shows an overview about all essential information of a user on the Arbalo platform:

* allocated [projects](/clients/projects)
* [project settings](#project-settings)
* [provisioned services](#provisioned-services)
* [collaborators](#collaborators)
* [billing](/clients/billing) and [usage](/clients/usage-monitoring)

![Dashboard](dashboard.png)

## Project Settings

Project settings is the place where you can administer your project. Currently, this consists of a list of the project's [client keys](/clients/client-keys).

## Provisioned Services

Provisioned services lists all services that are provisioned for the current project. A click on a service card shows the service detail view. This is similar information as in the [service catalog](/clients/service-catalog). The differences are:

* only the services are shown that are provisioned for the current project
* the connection url of the service instance is shown. This can be used in our application to address the service. You may copy this URL by clicking on the icon on the right side.
* metadata specific to your service instance is shown, e.g. the date when the service was provisioned or the service instance id.
* a service can be de-provisioned by clicking the red icon in the upper right corner.

![Provisioned Service Card](serviceCard.png)

## Collaborators

![Collaborators](collaborators.png)

Here, you may invite additional users into your project.

Invited users need to register with Arbalo.

All collaborators have access to same assets of the project they are collaborating on. These assets are:

* [project settings](#project-settings)
* [provisioned services](#provisioned-services)
* [collaborators](#collaborators)
* [billing](/clients/billing) and [usage](/clients/usage-monitoring)
