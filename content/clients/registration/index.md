---
title: "Registration and Login"
date: 2019-11-12T07:45:02+01:00
draft: false

weight: 10
---

## Registration

Registration is a prerequisite to use any of the services on the Arbalo platform.

Registration is personal, ie. we need to identify the person having access to the Arbalo platform. Please do not share your access credentials (e.g. user id and password).

Registration is needed in order to match the service usage to the user triggering this usage. Therefore, only registered users can use services on the Arbalo platform.

Additionally, we need identified users in order to bill for their service consumption.

That's why we collect the following data during registration:

* Name (firstname and lastname)
* Postal address
* E-Mail address
* Phone Number
* Payment information

Registration is only completed after successfully verifying the email address and the phone number (via SMS).

Of course, you only need to register once before starting working with Arbalo.

> Until end of 2019, Arbalo is in a closed beta test with first partners. Therefore, registration is not yet publicly available. Users are manually registered on request by the Arbalo team.

## Login

Registered Arbalo users can login based on user-id and password. The user's email address is used as her user-id.

The user-id must be unique within Arbalo. This means that users can open exactly one account per email address.

![Login Screen](login.png)

Users may check the *Remember me* checkbox. This enables caching of the credentials on the client side in order to make the login easier.

After successful login, the user is redirected to her [dashboard](/clients/dashboard).

## Password policy

Passwords must meet the following requirements:

* Minimum length 8 characters
* 1 Uppercase character
* 1 Lowercase character
* 1 digit
* Cannot have been used before

Arbalo does not store clear text passwords. Instead, a hash of your password is generated and stored to verify later login attempts.

Passwords are hashed with the `PBKDF2` algorithm and 50,000 iterations.

Two-factor authentication (2FA) is coming in the future.

## Password Reset

To reset or change your password you may click the button *Forgot Password?*

You will see a dialog where you can input your email address.

![Password Reset Screen](forgotPassword.png)

If the email address given is valid and saved under a registered user, an email with instructions is sent to that email address. Following these instructions in the received email, you may then create a new password and login with this new password.

## Current State

>Until end of 2019, Arbalo is in a closed beta test with first partners. The website, the service catalog and the whole platform are constantly evolving. We have the solution running in a development and test environment. Start of production is planned for beginning of 2020.

>Therefore, **registration is not yet open.**

> Please contact us if you are interested to contribute as a beta tester. We will then open a registration for you.
‍
> Of course, we are looking forward to hear from you directly on Social Media or via E-Mail info@arbalo.ch.
