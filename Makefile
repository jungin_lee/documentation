HUGO = hugo
FIREBASE_DEPLOY = firebase deploy --only hosting:default

default: build

build:
	$(HUGO)

ci-deploy:
	$(FIREBASE_DEPLOY) --token $(FIREBASE_TOKEN)

serve:
	$(HUGO) serve

clean:
	rm -rf public/*

.PHONY: build clean deploy-hosting
